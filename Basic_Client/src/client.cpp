#include <iostream>
#include <stdio.h>
#include <string>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <thread>


void listenForIncomingMessages(int socket) {
    char buffer[2048] = { 0 } ;
    std::string message ; 
    int dataLength = 0 ; 

    while(1) {
        dataLength = recv(socket , buffer , sizeof(buffer) , 0) ;
        message.append(buffer , dataLength) ;
        std::cout << "Server says: " << message << std::endl ;
        message.clear() ;
        memset(buffer , 0 , sizeof(buffer)) ;
    }
} ;



int main (int argc , char **argv) {

    struct addrinfo hints, *addrInfoList;
    int status ;
    char ipstr[INET6_ADDRSTRLEN] ;

    int clientSocket = 0 ;

    char clientName[1024] = { 0 } ;

    if(argc < 2) {
        std::cout << "Please provide the remote server's IP address and a client name...!\n" ;
        return 1 ;
    }
    else if (argc == 2) {
        std::cout << "Please provide a client name...!\n" ;
        return 1 ;
    }

    strncpy(clientName , argv[2] , sizeof(clientName)) ;

    memset(&hints , 0 , sizeof(hints)) ;
    // set internet address as IPv4.
    hints.ai_family = AF_INET ;
    // Set the protocol to TCP.
    hints.ai_socktype = SOCK_STREAM ; 

    status = getaddrinfo(argv[1] , "40000" , &hints , &addrInfoList) ;

    if (status != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        return 2;
    }

    // setup the socekt
    clientSocket = socket(addrInfoList->ai_family , addrInfoList->ai_socktype , addrInfoList->ai_protocol) ;
    if(clientSocket == -1) {
        std::cout << "Error while trying to create socket...\n" ;
        return 2 ;
    }
    
    inet_ntop(addrInfoList->ai_family , (struct sockaddr *)addrInfoList->ai_addr , ipstr , sizeof(ipstr)) ;
    // try connecting to remote server
    if(connect(clientSocket, addrInfoList->ai_addr, addrInfoList->ai_addrlen) == -1) {
        std::cout << "Error while trying to connect to server " << ipstr  << std::endl ;
        perror("Listen") ;
    }

    std::cout << "===============================\n" ;
    std::cout << "Connected to server with IP address: " << ipstr << std::endl ;
    std::cout << "===============================\n" ;


    //send the name of client to the server first
    send(clientSocket , clientName, sizeof(clientName) , 0) ;
     

    // create a thread to listen for incoming messages
     auto thread1 = std::thread(listenForIncomingMessages ,clientSocket) ;
     std::cout << "Ready to ghet messages as well..!!!\n" ;

    while(true) {
        
        std::string message ;
        int messageLength , lengthSent ;

        std::cout << "Please enter something to send to server: " << std::endl ;
        getline(std::cin , message) ;

        lengthSent = send(clientSocket , message.c_str(), message.size() , 0) ;

        if(lengthSent == message.size()) {
            std::cout << "Tick\n" ;
        }
        else {
            std::cout << "Error\n" ;
        }

        std::cout << "----------------------------\n" ;

    }

    
    freeaddrinfo(addrInfoList); // free the linked list

    thread1.join() ;

    return 0 ;
}
