# Chat Server
Simple C++ chat server based on sockets. This is the capstone project for the course [Udacity C++ Nanodegree Program](https://www.udacity.com/course/c-plus-plus-nanodegree).

## Dependencies for running locally
 This code base works on on Linux environments.

 - cmake >= 3.8
   - All OSes: [click here for installation instructions](https://cmake.org/install/)
 - make >= 4.1 
   - Linux: make is installed by default on most Linux distros
 - gcc/g++ >= 9
   - Linux: gcc / g++ is installed by default on most Linux distros
 - C++ 17 -> Minimum requirement to support C++ 17 are cmake 3.8 and g++ 9 (g++ 8 supports C++ 17 but has a bug that prevents code from running) 

## Basic Build Instructions

1. Clone this repo.
2. Make a build directory in the Server Directory : `mkdir -p build && cd build`
3. Compile: `cmake .. && make`
4. Run the `./tcp_server` 
5. Make a build directory in the Client Directory : `mkdir -p build && cd build`
6. Compile: `cmake .. && make`
6. Run the `./basic_client` and provide the server IP (127.0.0.1 for local connection) and a name for the client as arguments:
   for example: `./basic_client 127.0.0.1 client_1`

## Project Structure
```
ChatServer
|
|___README.md
|
|___Server
|   | 
|   |__headers  # Folder which contains header files
|   |  |_ Concurrent_PipeBased_Queue.h
|   |  |_ Concurrent_Queue.h
|   |  |_ Logger.h
|   |  |_ Message_Structures.h
|   |  |_ Multi_Queue_Reciever.h
|   |  |_ Network_Utils.h
|   |  |_ Tcp_Client.h
|   |  |_ Tcp_Client_Db_Manager.h
|   |  |_ Tcp_Client_Service_Manager.h
|   |  |_ Tcp_New_Connection_Acceptor.h
|   |  |_ Tcp_Server_Controller.h
|   |  |_ Thread_Guard.h
|   |
|   |__src # Includes implementation for classes defined in the headers folder
|   |  |
|   |  |_ main.cpp -> Includes code to instantiate the server and implement a simple terminal based menu system for the server
|   |
|   |__CMakeLists.txt
|   |
|   |__ README.md
|   |
|   |__.gitignore     
|
|___Basic_Client
    |
    |__src
    |  |
    |  |__client.cpp -> This file includes code for a simple client that is able to request a connection and send and recieve messages from the server.
    |
    |__CMakeLists.txt
 

```


## Implementation Details

The server is executable on Linux environments and the code base contains two parts:

1- The server code
2- A simple client to demonstrate the communication with the server.

The server is architected using queues and threads and conatins of four distinct parts:

1- The Server Controller: This part is responsible for communicating with the application and controlls the varoius parts of the server.
2- Connection Acceptor: This part is responsible for listening for new connection requeusts to the server and accepting them.
3- Client service Manager: This part of the code is responsible for managing a connection with a client and also sending and receiving messages with the client.
4- Data base: This part holds the information related to the clients.

The comunication section of the code with the clients uses systems based Polling functions.

## Criteria addressed

- The submission must compile and run. -> The two sections of the code (Server and Basic_Client) both compile and run.
- The project demonstrates an understanding of C++ functions and control structures. -> Switch and if statements have been used throughout the code such as in main.cpp, Tcp_Server_Contoller.cpp and Tcp_Client_Service_Manager.cpp.
- Classes use appropriate access specifiers for class members. -> All classes have specifiers, for example in all classes functions that are to be used internally are not exposed to the outside by making them private.
- Templates generalize functions in the project. -> Template are used to implement three different types of Queues that are used in the project. These can be find in the Concurrent_Queue.h, Concurrent_PipeBased_Queue.h and Multi_Queue_Reciever.h.
- The project uses destructors appropriately. -> Descructors of all classes that posses memory based variables are used to release such variables. An example is the Tcp_New_Connection_Acceptor.cpp file.
- The project uses scope / Resource Acquisition Is Initialization (RAII) where appropriate. -> In all files and thus classes resources are allocated and intialized when declared and released later.
- The project uses multithreading. -> The project uses multithreading extensively such that all the sub-units of the system (Controller , acceptor and client service) use worker threads to perfomr their functions.
- A promise and future is used in the project. -> Promise and futures are used as a mean for communicating the termination of a sub-unit successfully to the controller. An example can be seen in the Tcp_New_Connection_Acceptor.cpp file.
- A mutex or lock is used in the project. -> In this project , queues are used as a mean of communication between threads and mutexes are used for synchronization between the threads.
- A condition variable is used in the project. -> In this project , queues are used as a mean of communication between threads condition variables are used to notify threads and thus subunits that data is ready. Examples can be seen in Concurrent_Queue.h, Concurrent_PipeBased_Queue.h and Multi_Queue_Reciever.h .

## Expected Flow and outcome

When the server is executed a menu is shown with 5 options as below:
- By selecting the first option from the menu, the user can see the number of clients connected to the server.
- By selecting the second option from the menu, the user can see a list of of connected clients and their unique id.
- By selecting the third option from the menu, if there is any connected client, the user is prompted to enter the id of the client which they want to send a message and then the message they wish to send to that client.
- By selecting the fourth option from the menu, if there is any connected client, the user is prompted to enter the id of the client which they want to see its chat history and by selecting the target client, the chat history with that client is presented to the user.
- By selecting the fifth option from the menu, the program terminates by closing down the server and exiting from the program.

```

Please select from one of the following:
1 - get number of connections. 
2 - Show list of connected clients.
3 - Send a new message to a client.
4 - Show Chats with specific client. 
5 - Terminate Program
Your Choice:

```
 

If the basic_client executable is executed and the ip address of the server is provided as a command argument then a connection is created with the server.








