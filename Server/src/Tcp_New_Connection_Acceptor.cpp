#include "Tcp_New_Connection_Acceptor.h"


TcpNewConnectionAcceptor::TcpNewConnectionAcceptor(
    std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>> queue,
    std::shared_ptr<MessageQueue<std::string>> latest_queue ,
    TcpServerController *controller ,
    std::shared_ptr<Logger> logger
    ) 
{
    outBoundMsgQToController = queue ;
    latestNameQ = latest_queue ;
    this->logger = logger ;
    this->controller = std::unique_ptr<TcpServerController>(controller) ;
         
    if(pipe(threadPipeFd) == 0) {
        //initNetworkParams() ;
        logger->logVerbose("Connection Acceptor" , "Started"); 
    }
    else {
        logger->logError("Connection Acceptor" , "Error while creating pipes fpr communication") ;
    }   
}

// get sockaddr, IPv4 or IPv6:
void* TcpNewConnectionAcceptor::get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
} ;

bool TcpNewConnectionAcceptor::stopNewConnectionAcceptor() {
    bool terminate = true ;
    write(threadPipeFd[1] , &terminate , sizeof(terminate)) ;
    //this->workerThreadEndedFtr.wait_for(std::chrono::milliseconds(200));
    auto t1 = workerThreadEndedFtr.get() ;
    close(threadPipeFd[0]) ;
    close(threadPipeFd[1]) ;
    return true ;
} ;


void TcpNewConnectionAcceptor::infiniteLoop(std::promise<bool>&& pmr , int pipe_fd) {
    char ipstr[INET6_ADDRSTRLEN] ;

    int requesterSocketFd = 0 ;

    socklen_t requesterAddrSize = sizeof(requestersInfo);
    
    //struct sockaddr_storage requestersInfo; // incoming client's address information

    struct addrinfo hints, *addrInfoList ;

    memset(&hints , 0 , sizeof(hints)) ;
    // set internet address as IPv4.
    hints.ai_family = AF_INET ;
    // Set the protocol to TCP.
    hints.ai_socktype = SOCK_STREAM ; 

    //initNetworkParams() ;

    addressResolvingStatus = getaddrinfo(controller->getIpAddressPresentation() , "40000" , &hints , &addrInfoList) ;

    if ( addressResolvingStatus != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(addressResolvingStatus));
    }

    // setup the socekt
    if ((serverSocketFd = socket(addrInfoList->ai_family , addrInfoList->ai_socktype , addrInfoList->ai_protocol)) == -1) {
        perror("server: socket");
    }

    // set socket options
    if (setsockopt(serverSocketFd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
			perror("setsockopt");
	}

    // Try to bind socket
    if (bind(serverSocketFd, addrInfoList->ai_addr, addrInfoList->ai_addrlen) == -1) {
			close(serverSocketFd);
			perror("server: bind");    
    }

    // Start listening for connections 

    if (listen(serverSocketFd , SERVER_BACKLOG_QUEUE) == -1) {
        perror("listen");
    }
    std::string str ="Listening for connections on socketFd : [ " + std::to_string(serverSocketFd) + " ]" ;
    logger->logVerbose("Connection Acceptor" ,str) ;



    while(!terminateAcceptor) {

        struct pollfd fdList[2] ;
        memset(fdList , 0 ,sizeof(fdList)) ;

        // set up first slot with server socket
        fdList[0].fd = serverSocketFd ;
        fdList[0].events = POLLIN ;
        fdList[0].revents = 0 ;

        // set up first slot with pipe to recieve cancel message from controller
        fdList[1].fd = pipe_fd ;
        fdList[1].events = POLLIN ;
        fdList[1].revents = 0 ;

        requesterSocketFd = 0 ;
        
        int pollResult = poll(fdList , 2 , -1) ;
        

        if(pollResult > 0 ) {
            // If a cancel thread command is recieved
            int cancelCommandRecieved = fdList[1].revents & POLLIN;
            int connectionRequestRecived = fdList[0].revents & POLLIN;
            
            if(cancelCommandRecieved) {
                terminateAcceptor = true ;
            }
            else if (connectionRequestRecived) {
                socklen_t requesterAddrSize = sizeof(requestersInfo);
		        requesterSocketFd = accept(serverSocketFd, (struct sockaddr *)&requestersInfo, &requesterAddrSize);
		        if (requesterSocketFd == -1) {
			        perror("accept");
			        continue;
		        }
        
		        inet_ntop(requestersInfo.ss_family,
			    get_in_addr((struct sockaddr *)&requestersInfo), ipstr, sizeof(ipstr));
                std::string logStr("got connection request from [" + std::string(ipstr) + "][" + std::to_string(requesterSocketFd) + "]\n") ;
		        logger->logVerbose("Connection Acceptor" , logStr) ;
                

                // Logic to send the file descriptor of the new client to the controller.
                TcpClient* clientInfo = new TcpClient() ;
                clientInfo->clientsAddrInfo = requestersInfo ;
                clientInfo->requesterSocketFd = requesterSocketFd ;

                struct AcceptorToControllerMessage *acceptorToControllerPackage = new AcceptorToControllerMessage  ;
                acceptorToControllerPackage->msgType = AcceptorToControllerMsgType::NEW_CONNECTION_INFO ; 
                acceptorToControllerPackage->payload = (void *) clientInfo ;

                struct ControllerInboundMsgModel *controllerPackage  = new ControllerInboundMsgModel ;
                controllerPackage->source = ControllerInboundMsgSource::CONNECTION_ACCEPTOR ;
                controllerPackage->payload = (void *) acceptorToControllerPackage ;

                struct MultiQueueReceiverMsgStructure msg ;
                msg.msgType = MultiQueueReceiverMsgType::MESSAGE ;
                msg.payload = (void*) controllerPackage ; 
                
                outBoundMsgQToController->send(std::move(msg)) ;

                latestNameQ->send(std::move(outBoundMsgQToController->getQueueName())) ;

            }
            else 
            {
                // error
                logger->logError("Connection Acceptor" , "Error with Poll function");
            }
        }
    }
    
    // When out of while - send back true value to state that the client acceptor is stopped.
    pmr.set_value(true) ;
}


void TcpNewConnectionAcceptor::startNewConnectionAcceptor() {
    workerThreadEndedFtr = workerThreadEndedPmr.get_future() ;
    //infiniteLoop(std::move(this->workerThreadEndedPmr), threadPipeFd[0]) ;
    workerThread.executeThread( std::thread(&TcpNewConnectionAcceptor::infiniteLoop , this , std::move(workerThreadEndedPmr) , threadPipeFd[0]) ) ;
} ;

TcpNewConnectionAcceptor::TcpNewConnectionAcceptor(const TcpNewConnectionAcceptor&& other) {
     if(this != &other) {
          logger->logVerbose("Connection Acceptior" , "Moved");
     }
} ;

TcpNewConnectionAcceptor& TcpNewConnectionAcceptor::operator=(const TcpNewConnectionAcceptor&& other) {
   if(this != &other) {
      logger->logVerbose("Connection Acceptior" , "Moved");
   }
   return *this ;
} ;

TcpNewConnectionAcceptor::~TcpNewConnectionAcceptor() {
    (this->controller).release() ;
    logger->logVerbose("Connection Acceptior" , "Destroyed");
} ;



