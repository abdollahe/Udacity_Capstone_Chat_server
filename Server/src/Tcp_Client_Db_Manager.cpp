
/*                             In the name of Allah 
 * =====================================================================================
 *
 *       Filename:  TcpClientDbManager.cpp
 *
 *    Description: This file contains the implementation of the database class
 *                 responsible for holding the tcp clients information. 
 *
 *        Version:  1.0
 *        Created:  27/08/2022 
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Abdollah Ebadi, info@boundless-systems.com.au
 *   Organization:  Boundless-Systems PTY LTD
 *
 * =====================================================================================
 */

#include "Tcp_Client_Db_Manager.h"

TcpClientDbManager::TcpClientDbManager(std::shared_ptr<Logger> logger) {
   this->logger = logger ;
   this->logger->logVerbose("Database Manager" , "Created");
} ;


TcpClientDbManager::TcpClientDbManager(const TcpClientDbManager&& other) {
     if(this != &other) {
          this->tcpClientDb = other.tcpClientDb ;
          this->logger->logVerbose("Database Manager" , "Moved");
     }
} ;

TcpClientDbManager& TcpClientDbManager::operator=(const TcpClientDbManager&& other) {
   if(this != &other) {
      this->tcpClientDb = other.tcpClientDb ;
      this->logger->logVerbose("Database Manager" , "Moved");
   }
   return *this ; 
} ;

TcpClientDbManager::~TcpClientDbManager() {
   /* 
      If unique pointers are used to communicate with the controller we need 
      to release the reference to it in order to avoid a circular deletion of 
      resources and thus segmentation fault. If callbacks are used for communication the 
      line can be deleted.
   */ 
   //(this->controller).release() ;
   

   logger->logVerbose("Database Manager" , "Destroyed");
}; 

TcpClient* TcpClientDbManager::findTcpClient(int fd) {
   DbSearchResult result ;
   std::vector<TcpClient*>::iterator it ;
   it = std::find_if(tcpClientDb.begin() , tcpClientDb.end() , [fd](TcpClient const *obj) {
      return obj->requesterSocketFd == fd ;
   }) ;
   
   // std::vector<std::shared_ptr<TcpClient>>::iterator it ;
   // it = std::find_if(tcpClientDb.begin() , tcpClientDb.end() , [fd](TcpClient const *obj) {
   //    return obj->requesterSocketFd == fd ;
   // }) ;

   if(it != tcpClientDb.end()) {
      result.data = *it.base() ;
      result.found = true ;
   }
   else {
      result.data = nullptr ;
      result.found = false ;
   }

   return result.data ;
};

bool TcpClientDbManager::insertTcpClient(TcpClient* tcpClient) {
   bool returnValue = false ;
   try {
      tcpClientDb.emplace_back(tcpClient) ;
      listOfClientFds.emplace_back(tcpClient->requesterSocketFd) ;
      returnValue = true ;
   }
   catch (std::runtime_error e) {
      logger->logError("Database manager" , "Error while entering the client into DB - " + std::string(e.what())) ;
      returnValue = false ;
   }
   return returnValue ;
};

std::vector<int> TcpClientDbManager::getListOfClientFds() {
   return listOfClientFds ;
} ;

uint32_t TcpClientDbManager::getNumberOfConnectedClients() {
   return listOfClientFds.size() ;
}

