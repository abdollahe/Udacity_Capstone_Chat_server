#include "Tcp_Server_Controller.h"
#include <memory>
#include <thread>
#include <vector>
#include <algorithm>


enum class CommsType {
    SENT ,
    RECEIVED
} ;


struct CommsPackage {
    CommsType type ;
    std::string message ;
    std::string timeStamp ;
} ;

struct ClientCommsPackage {
    TcpClient *clientInfo ;
    std::vector<CommsPackage*> commsItem ;
} ;


class ChatDatabase {
    private:
    std::vector<struct ClientCommsPackage*> serverCommsHistory ;
    std::unordered_map<int , unsigned long> idToIndexMap ;
   
    std::string getCurrentTimeStamp() {
        auto currentTime = std::chrono::system_clock::now() ;
        auto timeStamp = std::chrono::system_clock::to_time_t(currentTime) ;
        auto timeStampString = std::string(std::ctime(&timeStamp)) ;

        return timeStampString ;

    };

    public:
    void messageRecieveCallback(TcpClient *client , std::string message) {
        addMessageToHistory(message , client , CommsType::RECEIVED) ; 
    };

    void addMessageToHistory(
        std::string message, 
        TcpClient* clientInfo, 
        CommsType commsType) 
    {
        // see if the client has comms stored in the db
        auto isClientInDb = isClientPresentInDb(clientInfo->requesterSocketFd) ;
        if(isClientInDb) {
            // If yes , retrieve the history
            auto index = idToIndexMap[clientInfo->requesterSocketFd];
            auto history = serverCommsHistory[index];
            
            // Create an item to add to the DB
            struct CommsPackage *commItem = new CommsPackage ;
            commItem->message = message ;
            commItem->type = commsType ;
            commItem->timeStamp = getCurrentTimeStamp() ;
            
            // Add to the DB
            history->commsItem.emplace_back(commItem) ;

        }
        else {
            // New client so add a new client
            
            // Create an item to add to the DB
            struct CommsPackage *commItem = new CommsPackage ;
            commItem->message = message ;
            commItem->type = commsType ;
            commItem->timeStamp = getCurrentTimeStamp() ;

            struct ClientCommsPackage *clientHistoryItem = new ClientCommsPackage ;
            clientHistoryItem->clientInfo = clientInfo ;
            clientHistoryItem->commsItem ;
            
            // Add new clients comms history to DB
            serverCommsHistory.emplace_back(clientHistoryItem) ;
            idToIndexMap[clientInfo->requesterSocketFd] = (serverCommsHistory.size() - 1) ;

        }
    }; 

    bool isClientPresentInDb(int id) {
        bool returnValue = false ;
        auto it = idToIndexMap.find(id) ;
        if(it != idToIndexMap.end()) {
            returnValue = true ;
        }
        
        return returnValue ;
    }

    void printChatHistoryWithClient(int clientId) {
        auto index = idToIndexMap[clientId] ;
        auto chatHistory = serverCommsHistory[index] ;
        if(clientId == chatHistory->clientInfo->requesterSocketFd) {
            
            std::cout << "Chat history with client: " << clientId << "\n" ; 
            for(auto item : chatHistory->commsItem) {
                if(item->type == CommsType::RECEIVED) {
                    std::cout << "Recieved -> " << item->timeStamp << " - " << item->message << std::endl ; 
                }
                else if (item->type == CommsType::SENT) {
                    std::cout << "Sent -> " << item->timeStamp << " - " << item->message << std::endl ; 
                }
            }
        }
        else {
            std::cout << "Error in fetching chat history with client: " << clientId << std::endl ;
        }
        
    } 



} ;

int main() {
    

ChatDatabase chatDb ;

std::string TAG = "Main Application" ;

// Step 1: Create an instance of the server
std::unique_ptr<TcpServerController> server1 = std::make_unique<TcpServerController>("127.0.0.1" , 3490 , "server1") ;

// Step 2: set the callback to receive messaged
server1->setNewMessageCallback([&chatDb](TcpClient *client , std::string message) {
   return chatDb.messageRecieveCallback(client , message) ;
}) ;

// Step 3: start the server so that it can listen for connections and accept them
server1->startServer() ;

int option = 0 ;
bool terminatProgram = false ;

std::cout << "===========================================\n\n\n" ;

while(!terminatProgram) {
    std::cout << "Please select from one of the following:\n" ;
    std::cout << "1 - get number of connections.\n" ;
    std::cout << "2 - Show list of connected clients.\n" ;
    std::cout << "3 - Send a new message to a client.\n" ;
    std::cout << "4 - Show Chats with specific client.\n" ;
    std::cout << "5 - Terminate Program\n" ;
    std::cout << "Your Choice: ";
    std::cin >> option ;
    
    std::cout << "--------------------\n" ;

    switch (option)
    {
        case 1:
        {   
            auto numberOfConnections = server1->getNumberOfConnectedClients() ;
            std::cout << "Number of connected clients to server: " << numberOfConnections << std::endl ;
            break ;
        }
        case 2:
        {   
            auto listOfClientIds = server1->getListOfConnectedClientIds() ;
            if(listOfClientIds.size() > 0 ) {
                for(int i = 0 ; i < listOfClientIds.size() ; i++) {
                auto item = listOfClientIds[i] ;
                std::cout << "Connected client " << i << " : id -> " << item << std::endl ;
                }
            }
            else {
                std::cout << "No clients connected to show list of ids.\n" ;
            }
            
            break ;
        }
        case 3:
        {
            int providedId = -1 ;
            //check that the id supplied is 
            if(server1->getNumberOfConnectedClients() > 0) {
                std::cout <<  "Please provide an id :" ;
                std::cin >> providedId ;
                std::cin.ignore() ;
                auto listOfConnectedIds = server1->getListOfConnectedClientIds();
                auto it = std::find(listOfConnectedIds.begin() , listOfConnectedIds.end() , providedId) ;
                if(it == listOfConnectedIds.end()) {
                    // not found - id invalid
                    std::cout << "The client id you entered is invalid, please try again..\n" ;
                }
                else {
                    // id found - id valid
                    std::string str ;
                    
                    std::cout << "Enter message to send:\n" << std::endl ;
                    getline(std::cin , str) ;
                
                    // Step 1 - Send message to client.
                    server1->sendMessageToClient(str , providedId) ;
                  
                    // Step 2 - Add the sent message to the chatDB.
                    auto clientInfo = server1->getClientInfo(providedId) ;
                    chatDb.addMessageToHistory(str , clientInfo , CommsType::SENT) ;
                }
            }
            else {
                std::cout << "No clients connected to send a message..\n" ;
            }
            
            break ;
        }
        case 4:
        {   
        
            int providedId = -1 ;
            //check that the id supplied is 
            if(server1->getNumberOfConnectedClients() > 0) {
                std::cout <<  "Please provide an id :" ;
                std::cin >> providedId ;
                std::cin.ignore() ;
                auto listOfConnectedIds = server1->getListOfConnectedClientIds();
                auto it = std::find(listOfConnectedIds.begin() , listOfConnectedIds.end() , providedId) ;
                if(it == listOfConnectedIds.end()) {
                    // not found - id invalid
                    std::cout << "The client id you entered is invalid, please try again..\n" ;
                }
                else {
                    chatDb.printChatHistoryWithClient(providedId) ;
                }
            }
            else {
                std::cout << "No clients connected to show chat history..\n" ;
            }
            
            
            break ;
        }
        case 5:
        {
            std::cout << "Starting server termination...\n" ;
            auto isServerTerminated = server1->stopServer() ;
            terminatProgram = isServerTerminated ;
            if(!isServerTerminated) std::cout << "Error while terminating server - refer to logs for more details\n" ;
            break ;
        }
        default:
        {   
            std::cout << "Please enter a valid option..!!\n" ;
            break ;
        }
    }   
    std::cout << "\n\n\n===========================================\n\n\n" ;   
}

std::cout << "Terminating program..\n" ;

return 0 ;
     
   
}