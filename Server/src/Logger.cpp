#include "Logger.h"


Logger::Logger(std::string file_name) : filePath(""){
    
    logTypeMapper[LogType::ERROR] = "Error" ;
    logTypeMapper[LogType::VERBOSE] = "Verbose" ;

    this->fileName = file_name ;
    
    this->inBoundLoggingMessagesQueue = std::make_unique<MessageQueue<LoggerPackage>>("loggerInBoundMessageQueue") ;

} ;

void Logger::setLogFilePath(std::string file_path) {
    filePath = file_path ;
}

Logger::~Logger() {
    logFile.close() ;
} ;

void Logger::logError(std::string responsible_entity , std::string logging_message ) {
    LoggerPackage msg ;
    msg.logType == LogType::ERROR ;
    msg.responsibleEntity = responsible_entity ;
    msg.loggingMessage = logging_message ;
    this->inBoundLoggingMessagesQueue->send(std::move(msg)) ;
}; 

void Logger::logVerbose(std::string responsible_entity , std::string logging_message ) {
    LoggerPackage msg ;
    msg.logType == LogType::VERBOSE ;
    msg.responsibleEntity = responsible_entity ;
    msg.loggingMessage = logging_message ;
    this->inBoundLoggingMessagesQueue->send(std::move(msg)) ;

} ;
void Logger::infiniteLoop(std::promise<bool> &&pmr) {
    while(!this->terminateLogger) {
        auto nextLog = this->inBoundLoggingMessagesQueue->recieve() ;
        
        if(nextLog.type == LoggerPackageType::LOG) {
           
            auto currentTime = std::chrono::system_clock::now() ;
            auto timeStamp = std::chrono::system_clock::to_time_t(currentTime) ;
            auto t2 = std::string(std::ctime(&timeStamp)) ;
            t2[t2.size() - 1] = ' ' ;
            std:: string logStr = t2 + " : " + logTypeMapper[nextLog.logType] + "/" + nextLog.responsibleEntity + " - " + nextLog.loggingMessage + "\n" ; 
            logFile << "==============================================================\n" ;
            logFile << logStr ; 
            
        }  
        else if (nextLog.type == LoggerPackageType::SHUT_DOWN) {
           
            this->_mtx.lock();
            this->terminateLogger = true;
            this->_mtx.unlock();
        }
    }
    pmr.set_value(true) ;
} ;


bool Logger::stopLogger() {
   LoggerPackage msg ;
   msg.type = LoggerPackageType::SHUT_DOWN ;
   this->inBoundLoggingMessagesQueue->send(std::move(msg)) ;
   auto isLoggerStopped = this->workerThreadFuture.get();
   
   return isLoggerStopped ;
} ;

void Logger::startLogger() {

     if(this->filePath == "") {
      std::filesystem::path cwd = std::filesystem::current_path() ;
      this->filePath =  cwd.string() ;
    }
    
    logFile.open(this->filePath + "/" + this->fileName , std::ios::out | std::ios::app) ;


    this->workerThread.executeThread(std::thread([&] { 
        this->workerThreadFuture = this->workerThreadPromise.get_future();
        this->infiniteLoop(std::move(this->workerThreadPromise)) ;
    })) ;
} ;