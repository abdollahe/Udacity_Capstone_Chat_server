#include "Tcp_Client_Service_Manager.h"


TcpClientServiceManager::TcpClientServiceManager(
   std::shared_ptr<PippedMessageQueue<ControllerToClientServiceMessage>> in_q ,
   std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>> out_q ,
   std::shared_ptr<TcpClientDbManager> dbManager ,
   std::shared_ptr<MessageQueue<std::string>> latest_queue , 
   std::shared_ptr<Logger> logger , 
   std::string server_name
) {
    
   this->serverName = server_name ;
   this->dbManager = dbManager ;
   this->logger = logger ;
   latestNamesQ = latest_queue ;
   inBoundMsgQFromController = in_q ;
   outBoundMsgQToController = out_q ;

   fdList = (struct pollfd*)malloc(sizeof(*fdList) * fdSize) ;

   this->logger->logVerbose("Client Service Manager" , "Created") ;

   
    
} ;


void TcpClientServiceManager::addToPollList(int new_fd) {
   
    if (fdCount == fdSize) {
        fdSize *= 2; 

        this->fdList = (struct pollfd*)realloc(this->fdList, sizeof(*fdList) * fdSize);
    }

    (this->fdList)[fdCount].fd = new_fd;
    (this->fdList)[fdCount].events = POLLIN;

    (fdCount)++;

} ;

void TcpClientServiceManager::removeFromPollList(int i) {
   // Copy the one from the end over this one
    fdList[i] = fdList[fdCount-1];

    (fdCount)--;
} ;

void TcpClientServiceManager::removeFromPollListUsingFd(int fd) {
      for(int i = 1 ; i < fdCount ; i++) {
         if(fdList[i].fd == fd) {
            removeFromPollList(i) ;
            break ;
         }
      }
} ;

TcpClientServiceManager::TcpClientServiceManager(const TcpClientServiceManager&& other) {
     if(this != &other) {
      this->logger->logVerbose("Client Service Manager" , "Moved") ;
     }
} ;

TcpClientServiceManager& TcpClientServiceManager::operator=(const TcpClientServiceManager&& other) {
   if(this != &other) {
     logger->logVerbose("Client Service Manager" , "Moved") ;
   }
   return *this ;
} ;

TcpClientServiceManager::~TcpClientServiceManager() {
    /* 
       If unique pointers are used to communicate with the controller we need 
       to release the reference to it in order to avoid a circular deletion of 
       resources and thus segmentation fault. If callbacks are used for communication the 
       line can be deleted.
     */ 
    //(this->controller).release() ;
    
   logger->logVerbose("Client Service Manager" , "Destroyed") ;
} ;

void TcpClientServiceManager::infiniteLoop(std::promise<bool>&& pmr , int pipe_fd) {
   // set up first slot with pipe to recieve command messages from controller
   fdList[0].fd = pipe_fd ;
   fdList[0].events = POLLIN ;
   fdList[0].revents = 0 ;
   fdCount++ ;
   
   while(!terminateServiceClient) {

      int pollResult = poll(fdList , fdCount , -1) ; 

      if(pollResult > 0 ) {
         
            // check to see if we are getting a command
            int controllerCommandRecieved = fdList[0].revents & POLLIN;

            if(controllerCommandRecieved) {
               
               //Flush the pipe of the message queue.
               bool y ; 
               read(fdList[0].fd , &y , sizeof(bool)) ;

               // read the command from the queue and perform act accordingly.
               auto commandMsg = inBoundMsgQFromController->recieve() ;
               auto commandType = commandMsg.commands ;

               switch (commandType) {
                  case ControllerCommandsToClientService::NEW_CLIENT: {
                     
                     auto payload = (TcpClient *)commandMsg.payload ;
                     addToPollList(payload->requesterSocketFd) ;
                     break;
                  }
                  case ControllerCommandsToClientService::SEND_MESSAGE: { 
                     auto payload = (ControllerSendMessageCommandToClientServiceModel *) commandMsg.payload ;
                     auto bytesSent = send(payload->clientFd, payload->message.c_str(),payload->message.size() , 0); 
                     if(bytesSent == -1) {
                        perror("sending error") ;
                        logger->logError("Client Service Manager" , "Error while sending data") ;
                     }
                     break ;
                  }    
                  case ControllerCommandsToClientService::CLOSE_CONNECTION: {  
                     auto payload = (CloseConnectionPackageStructure *) commandMsg.payload ;
                     // close the connection first
                     close(payload->clientFd) ;

                     // And remove the fd from the fd lis afterwards.
                     removeFromPollListUsingFd(payload->clientFd) ;
                     break; ;
                  }
                  case ControllerCommandsToClientService::SHUT_DOWN: {
                     
                     // first close all open connections.
                     for(int i = 0 ; i < fdCount ; i++) {
                        // close the connection first
                        close(fdList[i].fd) ;

                        // And remove the fd from the fd lis afterwards.
                        removeFromPollListUsingFd(fdList[i].fd) ;
                     } 

                     this->terminateServiceClient = true ;
                     break; ;
                  }      
               }  
            }
            else 
            {
         
               // run through the current connections to see if there is any data availalbe 
               for(int i = 1 ; i < fdCount ; i++) {
                  
                  if (fdList[i].revents & POLLIN) {
                    int numberOfBytes = recv(fdList[i].fd, buffer, sizeof(buffer), 0);

                    int senderFd = fdList[i].fd;

                    if (numberOfBytes <= 0) {
                        // Got error or connection closed by client
                        if (numberOfBytes == 0) {
                            // Connection closed
                            logger->logVerbose("Client Service Manager" , " Connection with " + std::to_string(senderFd) + " terminated") ;
                        } else {
                            perror("recv");
                        }

                        close(fdList[i].fd); 

                        removeFromPollList(i);

                    } 
                    else {
                        // We got some good data from a client
                        
                     
                        // Prepare the data to be sent back to the controller.
                        struct ClientServiceNewMessagePayloadStructure *clientNewMessagePackage = new ClientServiceNewMessagePayloadStructure ;
                        std::string str(buffer) ;
                        memset(buffer , 0 , sizeof(buffer)) ;
                        clientNewMessagePackage->message = str ;
                        
                        // find the client's info based on the fd.
                        auto searchResult = dbManager->findTcpClient(fdList[i].fd) ; 
                        if(searchResult != nullptr) {
                           clientNewMessagePackage->clientInfo = searchResult ;
                        }
                        
                        // Wrap the payload for controller to understand
                        struct ClientServiceToControllerMessage *clientToControllerPackage = new ClientServiceToControllerMessage ;
                        clientToControllerPackage->msgType = ClientServiceToControllerMsgType::NEW_MESSAGE ;                        
                        clientToControllerPackage->payload = (void *) clientNewMessagePackage ;

                        struct ControllerInboundMsgModel *controllerPackage  = new ControllerInboundMsgModel ;
                        controllerPackage->source = ControllerInboundMsgSource::CLIENT_SERVICE ;
                        controllerPackage->payload = (void *) clientToControllerPackage ;

                        // Send the packaged data to the controller. 
                        struct MultiQueueReceiverMsgStructure outBoundPackage ;
                        outBoundPackage.msgType = MultiQueueReceiverMsgType::MESSAGE ;
                        outBoundPackage.payload = (void *) controllerPackage ;

                        outBoundMsgQToController->send(std::move(outBoundPackage)) ;
                        
                        latestNamesQ->send(std::move(outBoundMsgQToController->getQueueName())) ;
                        
                        
                    }
                  }
               } 
            }
      } ;
   }
   
   // When out of while - send back true value to state that the client service is stopped.
   pmr.set_value(true) ;

} ;

bool TcpClientServiceManager::startClientServiceManager() {
   bool serviceClientRunning = true ;
   try {
      workerThreadEndedFtr = workerThreadEndedPmr.get_future() ;
      workerThread.executeThread(std::thread(&TcpClientServiceManager::infiniteLoop , this , std::move(workerThreadEndedPmr) , inBoundMsgQFromController->getReadFd())) ;
   }
   catch(...) {
      serviceClientRunning = false ;
   }
   return serviceClientRunning ;
    
} ;

bool TcpClientServiceManager::stopClientServiceManager()  {
   ControllerToClientServiceMessage package ;
   package.commands = ControllerCommandsToClientService::SHUT_DOWN ;
   inBoundMsgQFromController->send(std::move(package)) ;
   auto t1 = workerThreadEndedFtr.get() ;
   close(threadPipeFd[0]) ;
   close(threadPipeFd[1]) ;

   return t1 ;

};

