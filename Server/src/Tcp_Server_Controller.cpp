#include "Tcp_Server_Controller.h"

TcpServerController::TcpServerController(std::string ip_address, 
    uint16_t port_number, 
    std::string server_name )
{
    this->ipAddress = network_covert_ip_from_string(ip_address.c_str()) ;
    this->portNumber = port_number ;
    this->serverName = server_name ;

    logger = std::make_shared<Logger>("Server_Log.txt") ;
    logger->startLogger() ;

     // Initiate the multi queue reciever 
    queueReciever = std::make_unique<MultiQueueReceiver>(logger) ;

    lastestSenderNameQueue = std::make_shared<MessageQueue<std::string>>("latestSenderNameQueue") ;
    queueReciever->setLatestSenderQueue(lastestSenderNameQueue) ;


    // Initiate interface with application
    inBoundFromAppMsgQueue = std::make_shared<MessageQueue<MultiQueueReceiverMsgStructure>>("inBoundFromApplication") ;
    
    // Initiate the acceptor
    inBoundMsgQFromAcceptor = std::make_shared<MessageQueue<MultiQueueReceiverMsgStructure>>("inBoundFromAcceptor") ;
    tcpNewConnectionAcceptor = std::make_unique<TcpNewConnectionAcceptor>(
        inBoundMsgQFromAcceptor,
        lastestSenderNameQueue ,
        this,
        logger
        ) ; 
    
    // Initiate the db manager
    tcpDbManager = std::make_shared<TcpClientDbManager>(logger) ;

    // Initiate the client msg queues
    outBoundMsgQToClientService = std::make_shared<PippedMessageQueue<ControllerToClientServiceMessage>>("outBoundMsgQToClientService") ;
    outBoundMsgQToClientService->startQueue() ;

    inBoundMsgQFromClientService = std::make_shared<MessageQueue<MultiQueueReceiverMsgStructure>>("inBoundMsgQFromClientService") ;
    
    // initiate the client manager 
    clientManager = std::make_unique<TcpClientServiceManager>(
        outBoundMsgQToClientService, 
        inBoundMsgQFromClientService, 
        tcpDbManager,
        lastestSenderNameQueue,
        logger,
        serverName
    ) ;
    
    // add the in-bound queues for listening 
    queueReciever->addQueuesToWatch(inBoundFromAppMsgQueue , inBoundFromAppMsgQueue->getQueueName()) ;
    queueReciever->addQueuesToWatch(inBoundMsgQFromAcceptor , inBoundMsgQFromAcceptor->getQueueName()) ;
    queueReciever->addQueuesToWatch(inBoundMsgQFromClientService , inBoundMsgQFromClientService->getQueueName()) ;
} ;


void TcpServerController::setNewMessageCallback(std::function<void(TcpClient *, std::string)> callback) {
    this->newMessageCallback = callback ;
}

TcpServerController::TcpServerController(TcpServerController&& other) {
     if(this != &other) {   
        this->ipAddress = other.ipAddress ;
        this->portNumber = other.portNumber ;
        this->serverName = other.serverName ;

        this->tcpNewConnectionAcceptor = std::move(other.tcpNewConnectionAcceptor) ;
        
        this->logger->logVerbose("Server Controller" , "Moved");
     }
} ;

TcpServerController& TcpServerController::operator=(TcpServerController&& other) {
   if(this != &other) {
    this->ipAddress = other.ipAddress ;
    this->portNumber ;
    this->serverName ;

    this->tcpNewConnectionAcceptor = std::move(other.tcpNewConnectionAcceptor) ;
   
   this->logger->logVerbose("Server Controller" , "Moved");
   }
   return *this ;
} ;

TcpServerController::~TcpServerController() {
    logger->logVerbose("Server Controller" , "Destroyed");
} ;

char* TcpServerController::getIpAddressPresentation() {
    char* buffer ;
    char* returnVal = network_convert_ip_to_string(ipAddress , buffer) ;
    return returnVal ;
} ;

uint32_t TcpServerController::getIpAddress() {
    return ipAddress ;
}
        
uint16_t TcpServerController::getPortNumber() {
    return portNumber ; 
} ;
        
std::string TcpServerController::getServerName() {
   return serverName ;
} ;

void TcpServerController::startServer() {
    // start the network acceptor 
    tcpNewConnectionAcceptor->startNewConnectionAcceptor() ;

    // start the network client manager
    serviceClientWorking = clientManager->startClientServiceManager() ;

    // start thread for watching the multi reciever
    multiQueueRecieverThread.executeThread(std::thread( [&] { 
        multiQueueRecieverThreadEndedFtr = multiQueueRecieverThreadEndedPmr.get_future();
        queueReciever->watchQueues(std::move(multiQueueRecieverThreadEndedPmr)) ; 
            }
        )
    ) ;
    
    // Start controller thread 
    workerThread.executeThread(std::thread([&] { 
        workerThreadEndedFtr = workerThreadEndedPmr.get_future() ;
        infiniteLoop(std::move(workerThreadEndedPmr)) ;
    })) ;
   
} ;


void TcpServerController::infiniteLoop(std::promise<bool>&& pmr) {
    while(!terminateController) {
        auto inBoundMsg = queueReciever->resultQ->recieve() ;

        auto msg = (struct ControllerInboundMsgModel *) inBoundMsg.payload ;
        
        switch (msg->source)
        {
        case ControllerInboundMsgSource::APPLICATION:
            {
                auto message = (struct ApplicatonToControllerMsgModel*) msg->payload ;
                processApplicationInBoundMessages(message) ;
                break;
            }
        case ControllerInboundMsgSource::CONNECTION_ACCEPTOR:
            {
                auto message = (struct AcceptorToControllerMessage *) msg->payload ;
                processAcceptorInBoundMessages(message) ;
                break ;
            }
        case ControllerInboundMsgSource::CLIENT_SERVICE:
            {
                auto message = (struct ClientServiceToControllerMessage *)msg->payload ;
                processClientServiceInBoundMessages(message) ;
                break ;
            }
        }
    }
    // When out of while - send back true value to state that the client service is stopped.
    pmr.set_value(true) ;
} ;

void TcpServerController::processAcceptorInBoundMessages(struct AcceptorToControllerMessage *message) {
    if(message->msgType == AcceptorToControllerMsgType::NEW_CONNECTION_INFO) {

        auto clientInfo = (TcpClient *)message->payload ;

        // send the client info to the dabase manager for it to save it
        tcpDbManager->insertTcpClient(std::move(clientInfo)) ;
    
        // send the client to the service manager to initiate the communication with the client.
        struct ControllerToClientServiceMessage t1 ;
        t1.commands = ControllerCommandsToClientService::NEW_CLIENT ;
        t1.payload = (void *) clientInfo ;
        outBoundMsgQToClientService->send(std::move(t1)) ;
            
    }
} ;


void TcpServerController::processClientServiceInBoundMessages(struct ClientServiceToControllerMessage *message) {
    if(message->msgType == ClientServiceToControllerMsgType::NEW_MESSAGE) {
        //pack the incoming data and send it to the application.
        auto msg = (struct ClientServiceNewMessagePayloadStructure *) message->payload ;
        newMessageCallback(msg->clientInfo , msg->message) ;
    }
} ;

bool TcpServerController::stopServer() {
    // Stop the acceptor
    auto isAcceptorTerminated = tcpNewConnectionAcceptor->stopNewConnectionAcceptor() ;
    
    // Stop the client service manager
    auto isClientServiceTerminated = clientManager->stopClientServiceManager() ;
    
    // Stop the controller worker thread.  
    ApplicatonToControllerMsgModel *payloadPackage = new ApplicatonToControllerMsgModel ;
    payloadPackage->type = ApplicationToControllerMsgType::SHUT_DOWN ;
    
    ControllerInboundMsgModel *payload = new ControllerInboundMsgModel ;
    payload->source = ControllerInboundMsgSource::APPLICATION ;
    payload->payload = (void*) payloadPackage ;

    MultiQueueReceiverMsgStructure msg ;
    msg.msgType = MultiQueueReceiverMsgType::MESSAGE ;
    msg.payload = (void*) payload ;

    inBoundFromAppMsgQueue->send(std::move(msg)) ;
    lastestSenderNameQueue->send(std::move(inBoundFromAppMsgQueue->getQueueName())) ;

    auto isControllerWorkerTerminated = workerThreadEndedFtr.get() ;
    
    // Stop the multi reciver queue thread
    MultiQueueReceiverMsgStructure stopCommand ;
    stopCommand.msgType = MultiQueueReceiverMsgType::STOP_COMMAND ;
    inBoundFromAppMsgQueue->send(std::move(stopCommand)) ;
    lastestSenderNameQueue->send(std::move(inBoundFromAppMsgQueue->getQueueName())) ;
    auto isMultiQueueRecieverTerminated = multiQueueRecieverThreadEndedFtr.get() ;   
    
    auto isLoggerTerminated = logger->stopLogger() ;
   
    return isAcceptorTerminated && isClientServiceTerminated && isMultiQueueRecieverTerminated && isControllerWorkerTerminated && isLoggerTerminated ;
} ;

void TcpServerController::processApplicationInBoundMessages(struct ApplicatonToControllerMsgModel *message) {
    if(message->type == ApplicationToControllerMsgType::SHUT_DOWN) {
        terminateController = true ;
    }
    else if (message->type == ApplicationToControllerMsgType::NEW_MESSAGE) {
        
        auto msgFromApp = (ApplicationToControllerNewMessageModel *) message->payload ;

        struct ControllerSendMessageCommandToClientServiceModel *msg = new ControllerSendMessageCommandToClientServiceModel ;
        msg->clientFd = msgFromApp->clientId ;
        msg->message = msgFromApp->message ;
        
        struct ControllerToClientServiceMessage t1 ;
        t1.commands = ControllerCommandsToClientService::SEND_MESSAGE ;
        t1.payload = (void *) msg ;

        outBoundMsgQToClientService->send(std::move(t1)) ;
    }
}  ;


void TcpServerController::sendMessageToClient(std::string message , int id) {

    ApplicationToControllerNewMessageModel *appToCtrlMsgModel = new ApplicationToControllerNewMessageModel ;
    appToCtrlMsgModel->clientId = id ;
    appToCtrlMsgModel->message = message ;

    ApplicatonToControllerMsgModel *payloadPackage = new ApplicatonToControllerMsgModel ;
    payloadPackage->type = ApplicationToControllerMsgType::NEW_MESSAGE ;
    payloadPackage->payload = (void*) appToCtrlMsgModel ;
    
    ControllerInboundMsgModel *payload = new ControllerInboundMsgModel ;
    payload->source = ControllerInboundMsgSource::APPLICATION ;
    payload->payload = (void*) payloadPackage ;
    
    MultiQueueReceiverMsgStructure msg ;
    msg.msgType = MultiQueueReceiverMsgType::MESSAGE ;
    msg.payload = (void*) payload ;

    inBoundFromAppMsgQueue->send(std::move(msg)) ;
    lastestSenderNameQueue->send(std::move(inBoundFromAppMsgQueue->getQueueName())) ;

} ;


void TcpServerController::closeClientConnection(int id) {
    struct ControllerToClientServiceMessage t1 ;
    t1.commands = ControllerCommandsToClientService::CLOSE_CONNECTION ;
    struct CloseConnectionPackageStructure *msg = new CloseConnectionPackageStructure ;
    msg->clientFd = id ;
    t1.payload = (void *) msg ;
    outBoundMsgQToClientService->send(std::move(t1)) ;
} ;

uint32_t TcpServerController::getNumberOfConnectedClients() {
    return tcpDbManager->getNumberOfConnectedClients() ;
} ;

std::vector<int> TcpServerController::getListOfConnectedClientIds() {
    return tcpDbManager->getListOfClientFds() ;
} ;

TcpClient* TcpServerController::getClientInfo(int id) {
    return tcpDbManager->findTcpClient(id) ;
};





