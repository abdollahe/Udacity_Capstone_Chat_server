/*                        In the name of Allah
 * =====================================================================================
 *
 *       Filename:  network_utils.c
 *
 *    Description: This file contains routines to work with Network sockets programs 
 *
 *        Version:  1.0
 *        Created:  10/06/2020 04:15:57 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  ABHISHEK SAGAR (), sachinites@gmail.com
 *   Organization:  Juniper Networks
 *
 * =====================================================================================
 */

#ifndef __NETWORK_UTILS__
#define __NETWORK_UTILS__

//==============================
//     System header files
//==============================
#include <stdint.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <memory.h>

/* General Nw utilities */
char *
network_convert_ip_to_string(uint32_t ip_addr,
                        char *output_buffer);

uint32_t
network_covert_ip_from_string(const char *ip_addr);

#endif /* __NETWORK_UTILS__  */