/*                                In the name of Allah
 * =====================================================================================
 *
 *       Filename:  Logger.h
 *
 *    Description: This file contains code for a logger to log events in to a file
 * 
 *
 *        Version:  1.0
 *        Created:  29/11/2022 
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Abdollah Ebadi , info@boundless-systems.com.au
 *   Organization:  Boundless-Systems PTY LTD
 *
 * =====================================================================================
 */

#ifndef _LOGGER_
#define _LOGGER_

//==============================
//     System header files
//==============================
#include <iostream>
#include <thread>
#include <fstream>
#include <filesystem>
#include <unordered_map>
//==============================
//     Project header files
//==============================
#include "Concurrent_Queue.h"
#include "Thread_Guard.h"
#include <algorithm>


/*********************************************************************
  # Enum class that holds the different types in which the 
    logger can recieve data from its queue.
  # Types : 
    LOG -> Indicates that the in-bound data is to be logged.
    SHUT_DOWN -> Indicates that the in-bound data wants is a 
                 shut down command for the logger.        
**********************************************************************/
enum class LoggerPackageType {
    LOG,
    SHUT_DOWN
} ;

/*********************************************************************
    # Enum class that holds the types of logging to be performed on
      the incoming data.    
    # Types : 
        ERROR   -> Indicates the logging performed should be an Error
                   logging.
        VERBOSE -> Indicates the logging performed should be an Verbose
                   logging.
**********************************************************************/
enum class LogType {
    ERROR , 
    VERBOSE
} ;

/*********************************************************************
  # This struct is the data model for the logger. 
  # Members : 
    LoggerPackageType -> Indicates if the package holds data for 
                         logging or the package is a command.
    LogType           -> Indicates the type of logging to be done 
                         on the data.           
    responsibleEntity -> Indicates the entity which wants to log.
    loggingMessage    -> The message for logging. 
**********************************************************************/
struct LoggerPackage {
  LoggerPackageType type = LoggerPackageType::LOG ;
  LogType logType = LogType::VERBOSE ;
  std::string responsibleEntity ;
  std::string loggingMessage ;
} ;



class Logger {
  private:

  std::string fileName ;
  std::string filePath  ;
  std::ofstream logFile ;

  // This map is used for converting the log type in to string values during logging.
  std::unordered_map<LogType , std::string> logTypeMapper ;
     
  // This queue is the main contact point of other parts of the program to 
  // send data to the logger for loggin. 
  std::unique_ptr<MessageQueue<LoggerPackage>> inBoundLoggingMessagesQueue ;

    
  // This varialbe is used as a flag to run the logger thread.
  bool terminateLogger = false ;

  // Mutex to perform synchronization. 
  std::mutex _mtx ;

  // Thread to perform the logging in an async manner.
  ThreadGuard workerThread ;
    
  // These variables are used to confirm the termination 
  // of the logger back to the calling controller.
  std::future<bool> workerThreadFuture ;
  std::promise<bool> workerThreadPromise ;

    
  /*****************************************************************
    # Function which the logger thread executes.
    # Params : 
      pmr ->  Promise which is used as a termination 
              confirmation flag.
  ******************************************************************/
  void infiniteLoop(std::promise<bool> &&pmr) ;
    
    
  public:


  /*****************************************************************
    # Constroctor
    # Params : 
        file_name ->  Name of the log file to be used by 
                      the logger.
  ******************************************************************/
  Logger(std::string file_name) ;
    
  ~Logger() ;

  /*****************************************************************
    # Function log a error message
    # Params : 
      responsible_entity -> Indicates the entity which wants to log.
      logginlogging_messagegMessage    -> The message for logging. 
    ******************************************************************/
    void logError(std::string responsible_entity , std::string logging_message) ;

  /*****************************************************************
    # Function log a verbose message
    # Params : 
      responsible_entity -> Indicates the entity which wants to log.
      logginlogging_messagegMessage    -> The message for logging. 
    ******************************************************************/
    void logVerbose(std::string responsible_entity , std::string logging_message) ;
    
  /*****************************************************************
    # Function to set the path of the logging file.
    # Params : 
      file_path ->  Path to the log file
      If this is not set, the file will be created the 
      directory of the server execition file.
    ******************************************************************/
    void setLogFilePath(std::string file_path) ;
        
    
    /*****************************************************************
    # Function to start the logger.
    ******************************************************************/
    bool stopLogger() ;

    
    /*****************************************************************
    # Function to stop the logger.
    ******************************************************************/
    void startLogger() ;

};

#endif
