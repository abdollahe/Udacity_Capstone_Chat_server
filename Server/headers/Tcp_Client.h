#ifndef _TCP_CLIENT_
#define _TCP_CLIENT_


//==============================
//     System header files
//==============================
#include <cstdint>

class TcpClient {
    private: 
    public:
    TcpClient() { } ;
    struct sockaddr_storage clientsAddrInfo ;
    int requesterSocketFd ;
    uint32_t id ; 

    TcpClient(const TcpClient& other) {
        this->id = other.id ;
        this->clientsAddrInfo = other.clientsAddrInfo ;
        this->requesterSocketFd = other.requesterSocketFd ;
    } 
} ;

#endif