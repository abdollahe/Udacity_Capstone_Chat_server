/*                             In the name of Allah 
 * =====================================================================================
 *
 *       Filename:  TcpNewConnectionAcceptor.h
 *
 *    Description: This file contains the description of the acceptor class
 *                 responsible establishing tcp connection with clients.
 *
 *        Version:  1.0
 *        Created:  29/08/2022 
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Abdollah Ebadi, info@boundless-systems.com.au
 *   Organization:  Boundless-Systems PTY LTD
 *
 * =====================================================================================
 */
#ifndef _TCP_NEW_CONNECTION_ACCEPTOR_
#define _TCP_NEW_CONNECTION_ACCEPTOR_


//==============================
//     System header files
//==============================
#include <iostream>
#include <functional>
#include <memory>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <future>
#include <poll.h>
//==============================
//     Custom header files
//==============================
#include "Tcp_Client.h"
#include "Tcp_Server_Controller.h"
#include "Concurrent_Queue.h"
#include "Message_Structures.h"
#include "Multi_Queue_Reciever.h"
#include "Logger.h"


class TcpServerController ;


class TcpNewConnectionAcceptor {
    private:   

    std::unique_ptr<TcpServerController> controller ; 

     // Queue of messages outgoing to controller
    std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>> outBoundMsgQToController ;
    
    // Shared pointer to multi reciever queue's name queue so that 
    // the multi reciever queue can wake up.
    std::shared_ptr<MessageQueue<std::string>> latestNameQ ;

    ThreadGuard workerThread ;

    // Pointer to logger instance.
    std::shared_ptr<Logger> logger ;

    // Global flag for the worker thread's while loop to check 
    bool terminateAcceptor = false ;
        
    // Variables used for communication with the worker thread.
    std::promise<bool> workerThreadEndedPmr ;
    std::future<bool> workerThreadEndedFtr ;
    int threadPipeFd[2] ;

    //struct addrinfo hints, *addrInfoList ;
    int serverSocketFd = 0 ;
    int addressResolvingStatus ;
    char ipAddressPresentation[INET6_ADDRSTRLEN] ;
    int yes=1;
    int SERVER_BACKLOG_QUEUE = 10 ;

    struct sockaddr_in serverAddr;

    // incoming client's address information 
    struct sockaddr_storage requestersInfo; 

        
    /*****************************************************************
    # Function that is executed by the connection acceptor thread
    # Params : 
        pipe_fd ->  fd for write pipe to send shitdown command 
                    to the pooling mechanism used in service client.
        pmr -> Promise used to confirm the termination of the execution 
               thread back to the calling class.                     
    ******************************************************************/
    void infiniteLoop(std::promise<bool>&& pmr , int pipe_fd) ;


    /*****************************************************************
    # Function to get the IP.
    # Params : 
        sockaddr ->  Structure describing a generic socket address.                   
    ******************************************************************/
    void* get_in_addr(struct sockaddr *sa) ;

    public:

    TcpNewConnectionAcceptor(
        std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>> queue ,
        std::shared_ptr<MessageQueue<std::string>> latest_queue ,
        TcpServerController *controller ,
        std::shared_ptr<Logger> logger  
        ) ;

    
    /*****************************************************************
      # Function to start the connection acceptor after it has been
        instantiated.
    ******************************************************************/
    void startNewConnectionAcceptor() ;

    /*****************************************************************
      # Function to stop the connection acceptor. 
    ******************************************************************/
    bool stopNewConnectionAcceptor() ;

    // Copy Constructor are de-activated  
    TcpNewConnectionAcceptor(const TcpNewConnectionAcceptor& other) = delete ; 

    // Copy Operator are de-activated
    TcpNewConnectionAcceptor& operator=(const TcpNewConnectionAcceptor& other) = delete ;

    // Move Constructor
    TcpNewConnectionAcceptor(const TcpNewConnectionAcceptor&& other) ;

    // Move Operator 
    TcpNewConnectionAcceptor& operator=(const TcpNewConnectionAcceptor&& other) ;

    ~TcpNewConnectionAcceptor() ;

} ;

#endif

