/*          In the name of Allah
 * =====================================================================================
 *
 *       Filename:  MultiQueueReceiver.h
 *
 *    Description: This file contains a class that is able to monitor multiple queues
 *                 and recieve data from them in a blocking manner.
 *
 *        Version:  1.0
 *        Created:  23/11/2022 
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Abdollah Ebadi , info@boundless-systems.com.au
 *   Organization:  Boundless-Systems PTY LTD
 *
 * =====================================================================================
 */

#ifndef _MULTI_QUEUE_RECIEVER_
#define _MULTI_QUEUE_RECIEVER_

//==============================
//     System header files
//==============================
#include <iostream>
#include <thread>
#include <memory>
#include <vector>
#include <mutex>
#include <future>
#include <algorithm>
#include <deque>
//==============================
//     Project header files
//==============================
#include "Concurrent_Queue.h"
#include "Logger.h"

enum class MultiQueueReceiverMsgType {
    STOP_COMMAND ,
    MESSAGE 
} ;

struct MultiQueueReceiverMsgStructure {
   virtual ~MultiQueueReceiverMsgStructure() { } ; 
   MultiQueueReceiverMsgType msgType = MultiQueueReceiverMsgType::MESSAGE ; 
   void* payload ;
} ;

class MultiQueueReceiver {
    private:
    
    std::mutex _mtx ;
    std::vector<std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>>> qList ;
    std::unordered_map<std::string , unsigned long> idToIndexMap ;

    std::shared_ptr<MessageQueue<std::string>> latestSenderNameQ ;

    bool continueWatching = true ;

    std::shared_ptr<Logger> logger ;

    public:

    std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>> resultQ ;
    std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>> commandQ ;
    
    MultiQueueReceiver(std::shared_ptr<Logger> logger) {
        this->logger = logger ;
        
        resultQ = std::make_shared<MessageQueue<MultiQueueReceiverMsgStructure>>("resultQ") ; 
        commandQ = std::make_shared<MessageQueue<MultiQueueReceiverMsgStructure>>("commandQ") ;
        
        
        qList.emplace_back(commandQ) ;
        // add the id to the id to index map
        idToIndexMap[commandQ->getQueueName()] = (qList.size()-1) ;
        logger->logVerbose("MultiQueueReciever" , "Started") ;
    } ;
    
    ~MultiQueueReceiver() { 
        logger->logVerbose("MultiQueueReciever" , "Destroyed") ;
    } ;

    void setLatestSenderQueue(std::shared_ptr<MessageQueue<std::string>> queue) {
        this->latestSenderNameQ = queue ;
    } ;

    void addQueuesToWatch(std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>> queue , std::string queue_name) {
        qList.emplace_back(queue) ;
        // add the id to the id to index map
        idToIndexMap[queue_name] = (qList.size() - 1) ;
    }

    void removeQueuesFromWatch(std::string queue_name) { 
        auto index = idToIndexMap[queue_name] ;
        qList.erase(qList.begin() + index) ;
        idToIndexMap.erase(queue_name) ;
     } ;

    void watchQueues(std::promise<bool>&& pmr) {
        _mtx.lock() ;
        continueWatching = true ;
        _mtx.unlock() ;

        while(continueWatching) { 
            auto id = latestSenderNameQ->recieve() ;
            auto index = idToIndexMap[id] ; 
            auto targetQ = qList[index] ;
            auto msg = targetQ->recieve() ;
            
            if(msg.msgType == MultiQueueReceiverMsgType::STOP_COMMAND) {
                _mtx.lock() ;
                continueWatching = false ;
                _mtx.unlock() ;
            }
                
            else { 
                resultQ->send(std::move(msg)) ;
            }
        }
        pmr.set_value(true) ;
    }

    void stopWatching() {
        MultiQueueReceiverMsgStructure payload ;
        std::string *msg = new std::string("stop") ;
        payload.payload = (void *) msg ;
        commandQ->send(std::move(payload)) ;
        auto name = commandQ->getQueueName() ;
        latestSenderNameQ->send(std::move(name)) ;
    }

} ;

#endif