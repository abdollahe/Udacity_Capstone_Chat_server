/*                                In the name of Allah
 * =====================================================================================
 *
 *       Filename:  Message_Structures.h
 * 
 *
 *    Description: This file contains the data models used throughout the program
 *                 for communication purposes between the different sub-units.
 *
 *        Version:  1.0
 *        Created:  12/09/2022 
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Abdollah Ebadi , info@boundless-systems.com.au
 *   Organization:  Boundless-Systems PTY LTD
 *
 * =====================================================================================
 */


#ifndef _CONTROLLER_ACCEPTOR_MESSAGING_QUEUE_
#define _CONTROLLER_ACCEPTOR_MESSAGING_QUEUE_

//==============================
//     Custom header files
//==============================
#include "Tcp_Client.h"


/*********************************************************************
  # Enum class that holds the different types of commands the 
    controller can send to the client service manager
  # Types : 
    NEW_CLIENT-> Indicates the message holds info about a new client.
    SEND_MESSAGE-> Indicates the client service manager needs to 
                   send a message to a client.
    CLOSE_CONNECTION-> Indicates that the client service manager 
                       needs to close a connection.               
    SHUT_DOWN -> Indicates that the client service manger needs 
                 to shutdown.             
**********************************************************************/
enum class ControllerCommandsToClientService {
    NEW_CLIENT,
    SEND_MESSAGE,
    CLOSE_CONNECTION,
    SHUT_DOWN
} ;

/*********************************************************************
  # The values of this enum class show where the source of the messages 
    recived by the controller are. The controller gets its messages from 
    the multi queue reciever and we must be able to distinguish between 
    entities that  send data to 
    controler. This enum does that
  # Types : 
    APPLICATION-> Indicates the message is from the application side.
    CLIENT_SERVICE-> Indicates the message is from the client service.
    CONNECTION_ACCEPTOR-> Indicates the message is from the acceptor.              
**********************************************************************/
enum class ControllerInboundMsgSource {
   APPLICATION , 
   CLIENT_SERVICE ,
   CONNECTION_ACCEPTOR 
} ;



/*********************************************************************
  # The values of this enum class show where the source of the messages 
    recived by the controller are. The controller gets its messages from 
    the multi queue reciever and we must be able to distinguish between 
    entities that  send data to controler. 
  # Types : 
    APPLICATION-> Indicates the message is from the application side.
    CLIENT_SERVICE-> Indicates the message is from the client service.
    CONNECTION_ACCEPTOR-> Indicates the message is from the acceptor.              
**********************************************************************/

struct ControllerInboundMsgModel {
    ControllerInboundMsgSource source ; 
    void * payload ;
} ;

//===============================================
// Client Acceptor to Controller messaging classes
//===============================================

/*********************************************************************
  # The values in this enum descriobe the type of message being sent 
    to the controller from the acceptor.
  # Types : 
    NEW_CONNECTION_INFO-> Indicates the message holds info about a 
                          new client trying to connect.
**********************************************************************/
enum class AcceptorToControllerMsgType {
    NEW_CONNECTION_INFO,
} ;

/*********************************************************************
  # This struct is the data model for the messages going from 
    acceptor to controlelr. 
  # Members : 
    AcceptorToControllerMsgType -> Indicates if the type of message 
                                   being sent.
    payload -> A void pointer that points to the actual data being sent.
**********************************************************************/
struct AcceptorToControllerMessage {
  AcceptorToControllerMsgType msgType  ;
  void* payload ;
} ;


//===============================================
// Client Service to Controller messaging classes
//===============================================

/*********************************************************************
  # This struct is the data model for sending a new message recieved 
    by the client service manager back to the controller.
  # Members : 
    message -> A memory that holds the message.
    clientInfo -> Tcp infor related to the client that sent the message.
**********************************************************************/
struct ClientServiceNewMessagePayloadStructure {
  std::string message ;
  TcpClient *clientInfo ;
} ;

/*********************************************************************
  # The values in this enum descriobe the type of message being sent 
    to the controller from the Client Service.
  # Types : 
    NEW_MESSAGE-> This indicates that the message being sent back 
                  is a new message.
**********************************************************************/
enum class ClientServiceToControllerMsgType {
    NEW_MESSAGE,
} ;

/*********************************************************************
  # This struct is the data model for sending messages from the 
    client service to controller.
  # Members : 
    msgType -> Type of message.
    payload -> A void pointer that points to the actual data being sent.
**********************************************************************/
struct ClientServiceToControllerMessage {
  ClientServiceToControllerMsgType msgType  ;
  void* payload ;
} ;

//===============================================
// Controller to Client service messaging classes
//===============================================


/*********************************************************************
  # This struct is the data model for sending messages from the 
    Controller to the client service manager.
  # Members : 
    commands -> Type of message (command) going from controller to 
                client service.
    payload -> A void pointer that points to the actual data being sent.
**********************************************************************/
struct ControllerToClientServiceMessage {
  ControllerCommandsToClientService commands ;
  void* payload ;
} ;

/*********************************************************************
  # This struct is the data model for providing the client service 
    the message and the client id to send a new message to. 
  # Members : 
    clientFd -> Id of the destination client.
    message -> message to be sent to the destination client.
**********************************************************************/
struct ControllerSendMessageCommandToClientServiceModel {
  int clientFd ;
  std::string message ;
} ;

/*********************************************************************
  # This struct is the data model for providing the client service 
   the info to close a specific clients communication. 
  # Members : 
    clientFd -> Id of the destination client.
    message -> message to be sent to the destination client.
**********************************************************************/
struct CloseConnectionPackageStructure {
  int clientFd ;
} ;

//===============================================
// Application to Controller messaging structures
//===============================================


/*********************************************************************
  # The values in this enum descriobe the type of message being sent 
    to the controller from the Application.
  # Types : 
    SHUT_DOWN-> This type indicates a shut down command
**********************************************************************/
enum class ApplicationToControllerMsgType {
  SHUT_DOWN ,
  NEW_MESSAGE , 
  CLOSE_CONNECTION
} ;

/*********************************************************************
  # This struct is the data model for providing the controller the 
    new message requested by the application.
  # Members : 
    clientId -> The id of the client.
    message -> message to be sent to the destination client.
**********************************************************************/
struct ApplicationToControllerNewMessageModel {
  int clientId ;
  std::string message ;
} ;

/*********************************************************************
  # This struct is the data model for providing the controller
    a message
  # Members : 
    type -> Type of message to controller from application.
    message -> message to be sent to the destination client.
**********************************************************************/
struct ApplicatonToControllerMsgModel {
  ApplicationToControllerMsgType type ;
  void* payload ;
} ;

#endif