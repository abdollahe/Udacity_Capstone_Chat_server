/*                             In the name of Allah 
 * =====================================================================================
 *
 *       Filename:  TcpClientServiceManager.h
 *
 *    Description: This file contains the description of the client service class
 *                 responsible for sending and recieving data to and from the client.
 *
 *        Version:  1.0
 *        Created:  29/08/2022 
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Abdollah Ebadi, info@boundless-systems.com.au
 *   Organization:  Boundless-Systems PTY LTD
 *
 * =====================================================================================
 */

#ifndef TCP_CLIENT_SERVICE_MANGER
#define TCP_CLIENT_SERVICE_MANGER

//==============================
//     System header files
//==============================
#include <memory>
#include <iostream>
#include <functional>

//==============================
//     Custom header files
//==============================
#include <Tcp_Server_Controller.h>
#include "Concurrent_Queue.h"
#include "Concurrent_PipeBased_Queue.h"
#include "Message_Structures.h"
#include "Multi_Queue_Reciever.h"
#include "Tcp_Client.h"
#include "Logger.h"


class TcpServerController ;
class TcpClientDbManager ;
class TcpClient ;
class Logger ;


class TcpClientServiceManager {
    private: 
    
    // Queue of messages incoming from controller.
    std::shared_ptr<PippedMessageQueue<ControllerToClientServiceMessage>> inBoundMsgQFromController ;
    
    // Queue of messages outgoing to controller
    std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>> outBoundMsgQToController ; 
    
    // Shared pointer to database manager
    std::shared_ptr<TcpClientDbManager> dbManager ;
    
    // Shared pointer to multi reciever queue's name queue so that 
    // the multi reciever queue can wake up.
    std::shared_ptr<MessageQueue<std::string>> latestNamesQ ;

    // Pointer to logger instance.
    std::shared_ptr<Logger> logger ;
   
    std::string serverName ;

    ThreadGuard workerThread ;

    // Global flag for the worker thread's while loop to check 
    bool terminateServiceClient = false ;
        
    // Variables used for communication with the worker thread.
    std::promise<bool> workerThreadEndedPmr ;
    std::future<bool> workerThreadEndedFtr ;
    int threadPipeFd[2] ;

    // File descriptors for commucation with clients
    int fdCount ; 
    int fdSize = 5 ;
    struct pollfd *fdList ; 

    std::mutex _mtx ;

    // Temp buffer for client data
    char buffer[1024] ;    

    
    /*****************************************************************
      # Function that is executed by the service client thread
      # Params : 
        pipe_fd ->  fd for write pipe to send shitdown command 
                    to the pooling mechanism used in service client.
        pmr -> Promise used to confirm the termination of the execution 
               thread back to the calling class.                     
    ******************************************************************/
    void infiniteLoop(std::promise<bool>&& pmr , int pipe_fd) ;

    
     /*****************************************************************
      # Function to add the a new fd to the polling list of the service
        client manager.
      # Params : 
                new_fd ->  fd to be added.                         
    ******************************************************************/
    void addToPollList(int new_fd) ;
    
    /*****************************************************************
      # Function to remove an fd from the polling list using an index.
      # Params : 
                i ->  index of the fd to be removed.
    ******************************************************************/
    void removeFromPollList(int i) ;

     /*****************************************************************
      # Function to remove an fd from the polling list using the fd.
      # Params : 
                fd ->  file descriptor to be removed.
    ******************************************************************/
    void removeFromPollListUsingFd(int fd) ;
        
    public:
    TcpClientServiceManager(
        std::shared_ptr<PippedMessageQueue<ControllerToClientServiceMessage>> in_q,
        std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>> out_q , 
        std::shared_ptr<TcpClientDbManager> dbManager ,
        std::shared_ptr<MessageQueue<std::string>> latest_queue ,
        std::shared_ptr<Logger> logger ,  
        std::string server_name) ;    

    // Copy Constructor are de-activated  
    TcpClientServiceManager(const TcpClientServiceManager& other) = delete ; 

    // Copy Operator are de-activated
    TcpClientServiceManager& operator=(const TcpClientServiceManager& other) = delete ;

    // Move Constructor
    TcpClientServiceManager(const TcpClientServiceManager&& other) ;

    // Move Operator 
    TcpClientServiceManager& operator=(const TcpClientServiceManager&& other) ;


    ~TcpClientServiceManager() ;

    /*****************************************************************
      # Function to start the service client manager after it has been
        instantiated.
    ******************************************************************/
    bool startClientServiceManager() ;

    /*****************************************************************
      # Function to stop the service client manager. 
    ******************************************************************/
    bool stopClientServiceManager() ;

} ;

#endif 
