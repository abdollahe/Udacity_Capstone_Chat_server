#ifndef _TCP_SERVER_CONTROLLER_
#define _TCP_SERVER_CONTROLLER_

//==============================
//     Systems header files
//==============================
#include <iostream>
#include <memory>
#include <stdint.h>
#include <string>
#include <sstream>
#include <future>
#include <functional>
//==============================
//     Custom header files
//==============================
#include "Network_Utils.h"
#include "Thread_Guard.h"
#include "Message_Structures.h"
#include "Tcp_New_Connection_Acceptor.h"
#include "Concurrent_Queue.h"
#include "Concurrent_PipeBased_Queue.h"
#include "Tcp_Client_Db_Manager.h"
#include "Tcp_Client_Service_Manager.h"
#include "Multi_Queue_Reciever.h"
#include "Logger.h"


class TcpNewConnectionAcceptor ;
class TcpClientServiceManager ;
class TcpClientDbManager ;
class ThreadGuard ;
class AcceptorToControllerMessage ;
class ControllerToAcceptorMessage ;
class Logger ;


class TcpServerController {
    private:
    // Ip address of the tcp server
    uint32_t ipAddress ;
        
    // Port number associated to server
    uint16_t portNumber ;

    // name of server
    std::string serverName ;

    bool terminateServer ;
        
    ThreadGuard multiQueueRecieverThread ;
    std::promise<bool> multiQueueRecieverThreadEndedPmr ;
    std::future<bool> multiQueueRecieverThreadEndedFtr ;

    ThreadGuard workerThread ;
    std::promise<bool> workerThreadEndedPmr ;
    std::future<bool> workerThreadEndedFtr ;

    std::function<void(TcpClient* , std::string)> newMessageCallback ;

    std::unique_ptr<MultiQueueReceiver> queueReciever ;
    std::shared_ptr<MessageQueue<std::string>> lastestSenderNameQueue ;

    bool terminateController = false ;

    std::shared_ptr<Logger> logger ;
        
    //-------------------------------------------
    //      Acceptor section initialization
    //-------------------------------------------

    // Pointer to the acceptor
    std::unique_ptr<TcpNewConnectionAcceptor> tcpNewConnectionAcceptor ;
        
    std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>> inBoundMsgQFromAcceptor ;
        
    //-------------------------------------------
    //      DB Manager section initialization
    //-------------------------------------------
        
    // Pointer to the db manager
    std::shared_ptr<TcpClientDbManager> tcpDbManager ;

    //-------------------------------------------
    //   Client service section initialization
    //-------------------------------------------
        
    // Messaging queues with the client service class.
    std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>> inBoundMsgQFromClientService ;  
    std::shared_ptr<PippedMessageQueue<ControllerToClientServiceMessage>> outBoundMsgQToClientService ;  
     
    // Pointer to the client service manager
    std::unique_ptr<TcpClientServiceManager> clientManager ;
        
    bool serviceClientWorking = false ;

    //-------------------------------------------
    //   Application section Interface
    //-------------------------------------------
    std::shared_ptr<MessageQueue<MultiQueueReceiverMsgStructure>> inBoundFromAppMsgQueue ;
        
    //-------------------------------------------
    void infiniteLoop(std::promise<bool>&& pmr) ;

    void processAcceptorInBoundMessages(struct AcceptorToControllerMessage * message) ;

    void processClientServiceInBoundMessages(struct ClientServiceToControllerMessage *message) ;
        
    void processApplicationInBoundMessages(struct ApplicatonToControllerMsgModel *message) ;

    
    public: 
    TcpServerController(
        std::string ip_address, 
        uint16_t port_number, 
        std::string server_name) ;

    // Copy Constructor are de-activated  
    TcpServerController(TcpServerController& other) = delete ; 

    // Copy Operator are de-activated
    TcpServerController& operator=(TcpServerController& other) = delete ;

    // Move Constructor
    TcpServerController(TcpServerController&& other) ;

    // Move Operator 
    TcpServerController& operator=(TcpServerController&& other) ;

    ~TcpServerController() ;

    /*****************************************************************
    # Function to set the callback from the app which is called when
      a new message is recieved.
    # Params : 
        Function -> a function with TcpClient and string params.   
    ******************************************************************/
    void setNewMessageCallback(std::function<void(TcpClient * , std::string)> callback) ;
        
    /*****************************************************************
    # Function to get ip address of the server in string format.
    # Return Type: : 
        Char* that shows the ip in text format.
    ******************************************************************/
    char* getIpAddressPresentation() ;

    /*****************************************************************
    # Function to get ip address of the server.
    # Return Type: : 
        unsigned int that represent the ip in machine format.
    ******************************************************************/
    uint32_t getIpAddress() ;
        
    
    /*****************************************************************
    # Function to get port number assigned to the server.
    # Return Type: : 
        unsigned int that represents the port number of the server.
    ******************************************************************/
    uint16_t getPortNumber() ;
        
    /*****************************************************************
    # Function to get name of the server.
    # Return Type: : 
        string format that represents the name of the server.
    ******************************************************************/
    std::string getServerName() ;

    /*****************************************************************
    # Function to start the server.
    ******************************************************************/
    void startServer() ;

    /*****************************************************************
    # Function to stop the server.
    # Return Type: : 
        bool that states if the server has been stopped or not.
        (true -> stopped / false -> not stopped)
    ******************************************************************/    
    bool stopServer() ;

    /*****************************************************************
    # Function to get the number of clients connected.
    # Return Type: : 
        unsigned int which shows the number of connected clients.
    ******************************************************************/    
    uint32_t getNumberOfConnectedClients() ;

    /*****************************************************************
    # Function to get the list of connected clients.
    # Return Type: : 
        Vector of int that each int element represents the id of the 
        client connected.
    ******************************************************************/    
    std::vector<int> getListOfConnectedClientIds() ;

    /*****************************************************************
    # Function to get the TCP info of a client
    # Params:
        id -> represents the id of the client that we want to get its info.
    # Return Type: : 
       Pointer to the TCP info of the client.
    ******************************************************************/    
    TcpClient* getClientInfo(int id) ;

    /*****************************************************************
    # Function to send a specific message to a specific client.
    # Params:
        id -> represents the id of the client that we want to send a message.
        message -> Message to be sent to the client.
    
    ******************************************************************/    
    void sendMessageToClient(std::string message , int id) ;

    /*****************************************************************
    # Function used to close connection with a specific client.
    # Params:
        id -> represents the id of the client that we want to close the connection.
    ******************************************************************/    
    void closeClientConnection(int id) ;

        
        
} ;

#endif 