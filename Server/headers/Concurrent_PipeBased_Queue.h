/*                                  In the name of Allah
 * =====================================================================================
 *
 *       Filename:  Concurrent__PieBased_Queue.h
 *
 *    Description: This file contains a messaging queue that is thread safe
 *                 for thread communication and uses system pipes to notify the reading 
 *                 end that a data is ready. This queue is suitable to be used with system
 *                 level multiplexing mechanism such as Poll functions.
 *
 *        Version:  1.0
 *        Created:  11/10/2022 
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Abdollah Ebadi , info@boundless-systems.com.au
 *   Organization:  Boundless-Systems PTY LTD
 *
 * =====================================================================================
 */

#ifndef _CONCURRENT_POLL_BASED_QUEUE_
#define _CONCURRENT_POLL_BASED_QUEUE_

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <cstring>
#include <fcntl.h>
#include "thread"
#include "memory"
#include "vector"
#include "mutex"
#include "future"
#include "algorithm"
#include <deque>


template <typename T> class PippedMessageQueue {
private:
        std::mutex _mtx ;
        std::deque<T> _messages ;
        int _numberOfMessages ;
        int pipes_fd[2] ;
        std::string qName ;

    public:
       
        PippedMessageQueue(std::string qName) { 
            this->qName = qName ;
        }

        bool startQueue() {
            if(pipe(pipes_fd) < 0) {
                return false;
            }
            else {
                return true ;
            }
       }

       int numOfItemsInQueue() {
            return _numberOfMessages ;
            }

        int getReadFd() {
            return pipes_fd[0] ;
        }

        void send(T &&item) {
            // Lock the critical section
            std::lock_guard<std::mutex> myLock(_mtx) ;
            
            // Print out what you are doing.
            //std::cout << "Pushing Item: " << &item << std::endl ;

            // put the vehicle instance in to the vector
            _messages.emplace_back(std::move(item)) ;

            _numberOfMessages++ ;

            std::this_thread::sleep_for(std::chrono::milliseconds(10)) ;
            auto dataReady = true ;
            write(pipes_fd[1] , &dataReady , sizeof(dataReady)) ;
        }

        T recieve() {
            // Use a unique_lock 
            std::lock_guard<std::mutex> myLock(_mtx) ;

            std::this_thread::sleep_for(std::chrono::milliseconds(10)) ;

            // get the first item in the list
            auto item = _messages.back() ;

            _numberOfMessages-- ;

            // remove the retrieved item form the list
            _messages.pop_back() ;

            return item ;
        }
};

#endif
