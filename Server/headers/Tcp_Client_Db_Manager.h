/*                             In the name of Allah 
 * =====================================================================================
 *
 *       Filename:  TcpClientDbManager.h
 *
 *    Description: This file contains the description of the database class
 *                 responsible for holding the tcp clients information. 
 *
 *        Version:  1.0
 *        Created:  27/08/2022 
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Abdollah Ebadi, info@boundless-systems.com.au
 *   Organization:  Boundless-Systems PTY LTD
 *
 * =====================================================================================
 */

#ifndef TCP_CLIENT_DB_MANAGER
#define TCP_CLIENT_DB_MANAGER

//==============================
//     System header files
//==============================
#include <vector>
#include <functional>
#include <memory>
#include <iostream>
#include <algorithm>

//==============================
//     Custom header files
//==============================
#include "Tcp_Server_Controller.h"
#include "Tcp_Client.h"
#include "Logger.h"


class TcpServerController ;


struct DbSearchResult {
    TcpClient *data ;
    bool found ;
} ;

class TcpClientDbManager {
    private:
    //std::vector<std::shared_ptr<TcpClient>> tcpClientDb ;
    std::vector<TcpClient *> tcpClientDb ;
    std::vector<int> listOfClientFds ;

    std::shared_ptr<Logger> logger ;
    
    public:
    
    //TcpClientDbManager(TcpServerController* server_controller) ;
    TcpClientDbManager(std::shared_ptr<Logger> logger) ;
    
    // Move Constructor
    TcpClientDbManager(const TcpClientDbManager&& other) ;

    // Move Operator 
    TcpClientDbManager& operator=(const TcpClientDbManager&& other) ;

    ~TcpClientDbManager() ;

      
    // Copy Constructor is de-activated  
    TcpClientDbManager(const TcpClientDbManager& other) = delete ; 

    // Copy Operator is de-activated
    TcpClientDbManager& operator=(const TcpClientDbManager& other) = delete ;

   
    /*****************************************************************
      # Function to find a specific TcpClient from the DB
      # Params : 
                fd ->  specific file descriptior of the target tcp client of interest
      # Return Type:
                DbSearch Result          
    ******************************************************************/
    TcpClient* findTcpClient(int fd) ;
    
    /*****************************************************************
      # Function to insert TcpClient into the DB
      # Params : 
                tcpClient -> TcpClient data model holding a clients info
      # Return Type:
                bool (true -> success and false -> error)          
    ******************************************************************/
    bool insertTcpClient(TcpClient* tcpClient) ;

    
    /*****************************************************************
      # Function to get the list of file descriptiors for the 
        connected client.
      
      # Return Type:
                vector of ints. 
    ******************************************************************/
    std::vector<int> getListOfClientFds() ;

    
    /*****************************************************************
      # Function to get the number of clients stored in the DB
      
      # Return Type:
                Int -> numnber of connected clients stored in DB 
    ******************************************************************/
    uint32_t getNumberOfConnectedClients() ;
    


} ;

#endif
